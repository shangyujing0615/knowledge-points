# mac电脑

### Node.js版本管理工具 （n）

1. n 可以在不同的版本之间进行切换
2. n不支持windows系统
3. 安装：npm install -g n 
4. 查看n的版本：n —version
5. 安装最新的lts版本：n lts / sudo n lts
6. 安装制定版本：n 12.16.2 / sudo n 12.16.2
7. 版本切换：终端输入 n 
   * 可以看到安装的node版本
   * 使用键盘的上下键，选择想要使用的版本
   * 选好后直接按回车键，即可切换到多选的版本
8. 卸载制定的版本：sudo n 12.16.2


### 快捷键

1. control+command+space 表情符
2. command+option+i 打开控制台
3. command+shift+delete 清楚缓存
4. option+command+c 代码块 （markdown）
5. command+shift+n  无痕模式
6. ​


###sarafi 浏览器 

1. command++ 放大
2. command + - 缩小
3. command+ 0 恢复默认
4. command+上箭头 滚动到网页上面
5. command+下箭头 滚动到网页底部
6. command+option+i 打开控制台
7. ​

