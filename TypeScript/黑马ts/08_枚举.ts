/**
 * 枚举：enum
 * 使用枚举定义一些带名字的常量。 TypeScript支持数字的和基于字符串的枚举。
 * 枚举成员默认有值，且默认是从0开始自增的值
 * 枚举成员的值为数字称为【数字枚举】，
 * 枚举成员的值为字符串称为【字符串枚举】,字符串枚举没有自增长的特点，必须要给字符串枚举设置初始化的值
 */

// 定义一个枚举
 enum Direction {
  Up,
  Down ,
  Left,
  Right,
}
function enumFun(params:Direction) {
  console.log(Direction);
}
// 使用枚举【Direction.值】
enumFun(Direction.Down)
// 字符串枚举
enum Direction2 {
  Up ='up',
  Down = 'down' ,
  Left ='left',
  Right='right',
}