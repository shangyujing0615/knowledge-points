/**
 * 类型别名（自定义类型）：为任意类型起别名 
 * 使用场景：当一类型（复杂）被多次使用的时候就可以使用自定义类型简化该类型的使用
 * 使用type 来声明一个自定义类型
 */

// 比较复杂，重复使用(number | string)[]
let a :(number | string)[] = [1,'a','b',2]
let b :(number | string)[] = [1,2,3,4,'b','zhangsan']
// 简化：
type CustomType = (number | string)[]
let c :CustomType = [1,'a','b',2]
let d :CustomType = [1,2,3,4,'b','zhangsan']