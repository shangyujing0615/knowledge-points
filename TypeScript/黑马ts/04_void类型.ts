/**
 * 函数类型：指的是函数参数的类型和返回值的类型
 * 如果函数没有返回值，那么返回值的类型就是void
 */
function my(name:string):void{
  console.log('name==',name);
}
my('zhangsan')