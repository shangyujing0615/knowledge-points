
console.log(typeof 'hello TS!'); // string

let p = {x:1,y:2}
function typeofFun (num1: typeof p){

}
function typeofFun2 (num1: {x:number,y:number}){

}
// typeofFun2 与 typeofFun 其实是一样的
typeofFun({x:10,y:20})

let typeofFun3 : typeof p.x // number类型