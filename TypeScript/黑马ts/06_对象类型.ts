/**
 * 对象类型
 */
// 声明方法一：
let person :{name:string;age:number;sayHi(name:string):void;great():void } = { 
  name:'章三',
  age:18,
  sayHi(name){},
  great:()=>{}
}
// 声明方法二：
type customObject = {
  name:string
  age:number
  sayHi(name:string):void
  great():void
}
let person2:customObject ={
  name:'章三',
  age:18,
  sayHi(name){},
  great:()=>{}
}
console.log(person2);


// 可选属性
type customObject1 = {
  name:string
  age?:number
  sayHi(name:string):void
  great():void
}
let person3:customObject1 ={
  name:'章三',
  sayHi(name){},
  great:()=>{}
}
console.log(person3);

// 接口 interface
interface customObject2 {
  name:string
  age?:number
  sayHi(name:string):void
  great():void
}
let person4:customObject2 ={
  name:'章三',
  sayHi(name){},
  great:()=>{}
}
console.log(person4);

// 接口继承
interface pageIndex{
  page:number
  size:number
}
// interface codeIndex{
//   page:number
//   size:number
//   code:number
//   message:string
// }
interface codeIndex extends pageIndex{
  code:number
  message:string
}
let info:codeIndex = {
  code:200,
  message:'great',
  page:1,
  size:20
}
console.log(info);
