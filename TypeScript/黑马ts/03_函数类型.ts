/**
 * 函数类型：指的是函数参数的类型和返回值的类型
 */
// 单独指定返回值类型
function add(num11: number, num22:number):number{
  return num11 * num22
}
const add1 = (num1:number , num2:number ):number =>{
  return num1 * num2
}
add(2, 3)
add1(2, 3)
/**
 * 同时指定返回类型（参数和返回值类型），只适用于函数表达式
 * const add2 = () =>{} 正常箭头函数
 * (num1:number, num2:number)=>number 指定参数以及返回类型
 */
// 也可以这样去写
const add2:(num1:number, num2:number)=> number = (num1,num2) => {
  return num1+num2
}
// type constomType = (num1:number, num2:number)=>number 
// const add2:constomType = (num1, num2)=>{
//   return num1+num2
// }
console.log(add2(3,4)); // 7
