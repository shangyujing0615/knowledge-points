/**
 * any类型：
 * ts中不推荐使用any，会失去ts类型保护
 */

/**
 * 设置成any类型
 */
let obj:any ={ x: 0 }

// 访问不存在的属性不报错｜给不存在的属性赋值不报错
obj.aaa
obj.aaa = 10
obj()
/**
 * 隐士any
 * 声明一个变量不给赋值，也不指定默认值
 * 或者函数的参数不指定类型
 */
let a // any
function anyFun (num1){
// 参数num1 是any类型
}
