/**
 * 使用函数功能时，有的参数可传，有的可不传
 * 有必传参数和可传参数时，先写必传参数
 * 必传参数不能在可选参数后面
 * 可选参数后面加?
 */
function fun (num1:number,num2?:number,num3?:number):void{
  console.log(num1,num2,num3);
}
console.log(fun(1));
// console.log(fun(1,3));
// console.log(fun(1,2,3));

