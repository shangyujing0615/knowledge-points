class Person{
  age: number
  gender = "男" // 相当于gender是string类型，默认值是‘男’=> gender:string = '男' 【遵循能省则省原则选择前面的】
}

/**
 * 类的构造函数:实现实例属性初始化
 * constructor没有返回值类型
 */

class Student {
  name:string
  age:number
  sex:string
  constructor(name:string,age:number,sex:string){
    this.name = name 
    this.age = age
    this.sex = sex
  }
}
const student = new Student('张三',18,'男' )
console.log(student.age,student.name,student.sex);// 18 张三 男


/**
 * 实例方法的使用
 */

class Point{
  m = 10
  n = 20
  scale(z:number){
    this.m *= z 
    this.n *= z
  }
}
const ponit = new Point()
ponit.scale(10)
console.log(ponit.m,ponit.n); // 100 ,200 


/**
 * 继承：extends 和 implements
 * 在js中继承只有extends
 */

class School {
  grade(){
    console.log('This is grade');
  }
}
class Students extends School{
  brak(){
    console.log('This is students!!!');
    
  }
}
const students = new Students()
console.log(students.grade()); // This is grade
console.log(students.brak()); // This is students!!!

interface Students1{
  sings():void
  name:string
}
class teachers implements Students1 {
  sings(){
    console.log('this is class tearchers');
  }
  name = '章三';
} 