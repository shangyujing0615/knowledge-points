/**
 * 数组的定义：（更推荐使用第一种数组定义的方法)
 * 1. 类型[] eg: number[] 数字类型数组 boolean[] 布尔类型数组
 * 2. Array<类型> eg: Array<string> 字符串类型数组
 */
let numbers: number[] = [1,2,3]
let numbers1:Array<string> = ['a','b','c']
let numbers2: boolean[] = [true,false]
/**
 * 【 | 】竖线在ts中叫做【联合类型】
 * (number | string)[]含义是一个数组可以有number，也可以有string
 * number | string[] 含义是一个string数组类型或者是一个number类型
 * 注意括号的含义不一样
 */
let numbers3:(number | string)[] = []