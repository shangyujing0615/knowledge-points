
/**
 * 类成员的可见性
 * public 公有的 （默认的，可以直接省略）
 * proected 受保护的 （仅对其声明的所在类和子类（非实例对象）中可见）
 * private 私有的(只在当前类中使用，子类和非实例对象)
 */
class School1 {
  grade(){
    console.log('This is grade');
  }
  protected move(){
    console.log('这个是受保护的');
  }
  private move1(){
    console.log('这个是私有的');
  }
}
class Students1 extends School1{
  brak(){
    console.log('This is students!!!');
    this.move()
  }
}
const students1 = new Students1()
console.log(students.grade()); // This is grade
console.log(students.brak()); // This is students!!!