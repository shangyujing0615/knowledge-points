// 引入一个包
const path = require('path')
// 引入html插件
const HTMLWebpackPlugin = require('html-webpack-plugin')
// 引入clean插件
const {CleanWebpackPlugin} = require('clean-webpack-plugin')
// webpack中的所有的配置信息都应该写在module.exports中
module.exports = {
  // entry 指定入口文件
  entry:'./src/index.ts',
  // output 指定打包文件所在目录
  output:{
    // 指定打包文件的目录
    path:path.resolve(__dirname, 'dist'),
    // 打包后文件的文件
    filename:'bundle.js',
    // 告诉webpack不使用箭头
    environment:{
      arrowFunction:false
    }
  },
  // 指定webpack打包时要使用模块
  module:{
    rules:[
      {
        // test 指定的事规则生成的文件
        test:/\.ts$/,
        // 要使用的loader
        use:[
          // 
          {
            loader:'babel-loader',
            options:{
              // 设置预定义的环境
              presets:[
                // 指定环境的插件
                [
                  "@babel/preset-env",
                  // 配置信息
                  {
                    // 要兼容的目标浏览器
                    targets:{
                      "chrome":'80'
                    },
                    // 指定corjs 的版本
                    "corejs":"3",
                    // 使用corejs的方式”usage“表示按需加载
                    "useBuiltIns":"usage"
                  }
                ]
              ]
            }
          },'ts-loader'],
        // 排除的文件
        exclude:/node-modules/
      }
    ]
  },
  // 配置webpack插件
  plugins:[
    new CleanWebpackPlugin(),
    new HTMLWebpackPlugin()
  ],
  // 设置引用模块
  resolve:{
    extensions:['.ts', '.js']
  }
}