/**
 * ts的类型
 * 1. string
 * 2. number
 * 3. boolean
 * 4. 字面量 
 * 5. any 表示任意类型，一个any设置any后，相当于对该变量关闭了TS的类型检测，使用ts时，不建议使用any类型
 * 6. unkonw 表示未知类型的值 类型安全的any
 * 7. as 类型 // 类型断言，告诉解析器变量的实际类型
 * 8. viod 空值undefined
 * 9. never 没有值
 * 10. object 
 * 11. function
 * 12. array
 * 13. 元组
 * 14.enum 枚举
 */



// 声明一个变量a，同时指定他的类型number
  // let a: number;
  // a = 10 
  // a = 20 
  // a = 'hello' // 直接报错，上面声明a的类型是number，不可以赋值字符串，报错仍然可以正常编译
  // console.log(a);
  

  // let b: string;
  // b = 'hello'

  // 声明完变量之后直接赋值
  // let c:boolean = true 
  // 如果变量的声明和赋值是同时进行的，TS可以自动对变量进行检测
  // let c = false  // ts默认认为c是boolean

  /**
    函数：参数类型直接在参数后面绑定指定的类型， 返回值类型在参数括号后面指定返回值的类型
    function sum(a:number, b:number):number{
      return a + b 
    }
    sum(10, 20)
   */
  
  // let d :'hello'
  // d = 'world' // 直接报错，上一行代码直接想当于 d = hello ,一般不这样使用
  // let d: boolean | string;
  // d = true
  // d = 'hello'

  /**
   * let d; //只声明一个变量不指定类型，则TS解析器自动判断变量的类型为any（隐式的any）
   * d = 10;
   * d = 'hello'
   * d = true
   */
  
  // let e:unknown;
  // e = 10 ;
  // e = 'hello';
  // e = true;

  /**
   *  类型断言，告诉解析器变量的实际类型
   *  as 类型 
   *  <类型>变量 
   *
   */
  // let s:string
  // s = e // 报错，s只接受string，e是unknow
  // s = e as string // 正常
  // s = <string>e // 正常

  /**
    * void 用来表示空，以函数为例，就表示没有返回值的函数
      function fn():void{
        
      }
      // never 表示永远没有返回结果
      function fn2():never{
        throw new Error('报错了')
      }
  */
  
  /**
   * {}用来指定对象中可以包含哪些属性
   * 语法：{属性名：属性,属性名?:属性,[propName:string]:any}
   * 在属性名后面加上？表示属性是可选的,[propName:string]:any表示任意类型的属性
   */
  let f : {name : string,age?:number}
  f = {name:'zhangsan',age:18}
  f = {name:'lisi'}
  /**
   *  ｜ 表示或
   *  & 表示同时
   */
     let l : {name:string} & {age: number}
     l = {name:'zhangsan',age:20}
  /**
   * 设置函数结构的类型声明：
   * 语法：(形参：类型；形参：类型...)=>返回值
   * let g:(a:number,b:number)=>number
   * 表明，g有俩参数a是number,b是number,返回值也是number
   */
  let g:(a:number,b:number)=>number
  g = function(n1:number,n2:number):number{
    return n1+n2
  }

  /**
   * Array
   *  类型【】
   *  Array<类型> 
   */
  let h:string[]
  h = ['1','2','3']
  let i: Array<number>; 
  i = [1,2,3]

  /**
   * 元组：固定长度的数组
   * 语法：[类型，类型，类型...]
   * 
   */
  let j:[string, string,number]
  j = ['hello','world',1]

  /**
   * enum 枚举 在多个值中进行选择
   */
  enum Gender{
    male = 0 ,
    Female = 1
  }
  let k:{name:string,gender:Gender}
  k = {
    name:"孙悟空",
    gender:Gender.male
  }


  /**
   * 类型的别名，简化类型的使用
   */
  type myType = 1|2|3|4|5;
  let o: myType
  let p : myType
  o = 5 
  // p = 6 // error

