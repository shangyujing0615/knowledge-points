/**
 * ts的类型
 * 1. string
 * 2. number
 * 3. boolean
 * 4. 字面量
 */
// 声明一个变量a，同时指定他的类型number
// let a: number;
// a = 10 
// a = 20 
// a = 'hello' // 直接报错，上面声明a的类型是number，不可以赋值字符串，报错仍然可以正常编译
// console.log(a);
// let b: string;
// b = 'hello'
// 声明完变量之后直接赋值
// let c:boolean = true 
// 如果变量的声明和赋值是同时进行的，TS可以自动对变量进行检测
// let c = false  // ts默认认为c是boolean
/**
  函数：参数类型直接在参数后面绑定指定的类型， 返回值类型在参数括号后面指定返回值的类型
  function sum(a:number, b:number):number{
    return a + b
  }
  sum(10, 20)
 */
// let d :'hello'
// d = 'world' // 直接报错，上一行代码直接想当于 d = hello ,一般不这样使用
var d;
d = true;
d = 'hello';
