/**
 * ts中的函数的定义
 * 1. 函数的定义
 * 2.可选参数
 * 3.默认参数
 * 4.剩余参数
 * 5.函数重载
 * 6.箭头函数
 */
/**
 * 1.函数的定义
 */
// ES5定义函数
/*
    // 函数的声明
    function run(){
        return 'run'
    }
    // 匿名函数
    let run = function(){
        return 'run2'
    }
*/
// ts定义函数
// 1. 函数声明法：
function fun():string{
    return 'run'
}
fun()
// 2.匿名函数
let fun2 = function():number{
    return 123
}
alert(fun2())
// ts中定义方法穿参 声明法：
function getInfo(name:string,age:number):string{
    return `姓名：${name},年龄：${age}`
}
console.log(getInfo('孙误空',20))
// ts中定义方法穿参 匿名法：
let fun3 = function(name:string,age:number):string{
    return `姓名：${name},年龄：${age}`
}
console.log(fun3('孙误空',20))
// 没有返回值的：
let fun4 = function(_name:string,_age:number):void{
    console.log('运行fun4');
}
fun4('孙误空',20)
/**
 * 2.可选参数：
 * 注意：可选参数必须配置到参数的后面
 * (name:string,age?:number)是正确的
 * (age?:number,name:string)是错误的
 */
// es5中里面方法的实参和形参可以不一样，但是在ts中必须一样，如果不一样，就需要配置可选参数

function getInfo2(name:string,age?:number):string{
    if(age){
        return  `姓名${name},年龄${age}`
    }else{
        return `姓名${name},年龄保密`
    }
}
console.log(getInfo2('孙尚香'));
/**
 * 3.默认参数
 */
// es5中没有办法设置默认参数，es6和ts中都可以配置默认参数
function getInfo3(name:string,age:number = 50):string{
    if(age){
        return  `姓名${name},年龄${age}`
    }else{
        return `姓名${name},年龄保密`
    }
}
console.log(getInfo3('孙尚香',30));
/**
 * 4.剩余参数
 */
function sum(a:number,b:number,c:number,d:number):number{
    return a+b+c+d
}   
console.log(sum(10,20,30,40));
// 三点运算符(ES6)：接收新参传过来的值
function sum2(...result:number[]):number{
    var sum = 0
    for (let index = 0; index < result.length; index++) {
        sum+=result[index]
    }
    return sum
}
console.log(sum2(1,2,3,4,5,6));
/**
 * 5.函数重载
 */
// ts中的重载，通过同一个函数提供多个函数类型定义来试下多种功能的目的
function getInfo4(name:string):string
function getInfo4(age:number):number
function getInfo4(str:any):any{
    if(typeof str === 'string'){
        return `我叫${str}`
    }else {
        return `我${str}岁了`
    }
}
console.log(getInfo4('孙尚香')); // 正确
console.log(getInfo4(30));// 正确
// console.log(getInfo4(true));// 错误
/**
 * 箭头函数 this指向上下文
 */
setTimeout(()=>{
    console.log('run')
},1000)

