/**
 * ts 中的类：
 * 1.类的定义
 * 2.继承
 * 3.类里面的修饰符
 * 4.静态属性 静态方法
 * 5.抽象类 继承 多态
 */
/**
 * 类的定义
 */

// 1. 定义类
class Person{
    name:string; // 属性 前面省略了public
    constructor(n:string){ // 构造函数 实例化类的时候触发的方法
        this.name = n
    }
    run():void{
        console.log(this.name)
    }
}
// 实例化
let p = new Person('zhangsan')
p.run()
// 2. ts定义类
class Person2{
    name:string; // 属性 前面省略了public
    constructor(n:string){ // 构造函数 实例化类的时候触发的方法
        this.name = n
    }
    // 获取nane
    getName():string{
        return this.name
    }
    // 设置
    setName(name:string):void{
        this.name = name
    }
}
// 实例化
let p1 = new Person2('张三')
console.log(p1.getName());
p1.setName('里斯')
console.log(p1.getName());

/**
 * 继承：
 * 1. extends
 * 2. super
 * 
 */
class Person3 {
    public  name:string
    constructor(name:string){
        this.name = name
    }
    run():string{
        return `${this.name}在运动`
    }
}
// var p2 = new Person3('王武')
// console.log(p2.run());
//  继承
class web extends Person3 {
    constructor(name:string) {
        super(name) // 初始化父类的构造函数
    }
    run():string{
        console.log(`子类--${this.name}在工作`);
        return this.name
    }
}
var w = new web('李四')
console.log(w.run());


/**
 * 类里面的修饰符:
 * ts定义属性的时候给我们提供了三种修饰符
 * public:公有，在类里面，子类，类外面都可以访问
 * protected：保护类型，在类里面，子类里面都可以访问，在类外部没法访问
 * private：私有 在类里面可以访问，子类，类外部都没法访问
 * 属性如果不加属性符，默认是公有 public
 */
 class Person4 {
    protected  name:string
    constructor(name:string){
        this.name = name
    }
    run():string{
        return `${this.name}在运动`
    }
}
// var p2 = new Person4('王武')
// console.log(p2.run());
//  继承
class web1 extends Person4 {
    constructor(name:string) {
        super(name) // 初始化父类的构造函数
    }
    run():string{
        console.log(`子类--${this.name}在工作`);
        return `子类--${this.name}在工作`
    }
}
var w1 = new web1('李四')
console.log(w1.run());
/**
 * 静态属性，静态方法
 */
/**
 * 抽象类 继承 多态
 */