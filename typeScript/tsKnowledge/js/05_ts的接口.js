"use strict";
/**
 * ts的接口：
 * 接口的作用：在面向对象的编程中，接口是一种规范的定义，他定义了行为和动作的规范，在程序设计里面，接口
 * 起到一种限制和规范的作用。接口定义了某一批类所需要遵守的规范，接口不关心这些类的内部状态数据，也不关
 * 心这些类的内部状态数据，也不关心这些类里方法的实现细节，它只规定这批类里必须提供某些方法，提供这些方
 * 法的类就可以满足实际需要，ts中的接口类似于java，同时增加了更灵活的接口类型，包括属性，函数，可缩阴和类等
 * 1. 属性接口
 */
/**
 * 属性接口：对JSON的约束
 */
// 1. ts中定义方法
function printLabel() {
    console.log('printLabel');
}
printLabel();
// 2.ts中定义传入参数
function printLabel2(label) {
    console.log('printLabel2');
    console.log(label);
}
printLabel2('hhaha');
// 3.ts中自定义方法传入参数对JSON进行约束
function printLabel3(labelInfo) {
    console.log(labelInfo.label);
}
printLabel3({ label: 'hhh' });
function printName(name) {
    // 必须传入参数 firstName seconfName
    console.log(name.firstName + '---' + name.secondName);
}
printName({ firstName: '张', secondName: '三' }); // 正确
let obj = {
    firstName: '损',
    secondName: '悟空',
    age: 20
};
printName(obj); // 正确
function personInfo(info) {
    console.log(info);
}
personInfo({ firstName: '张', secondName: 'san' });
