"use strict";
/**
 * ts的基本类型：(ts为了使编写的代码更加规范，更有利于维护，增加类型校验)
 * 1.布尔类型(boolean)
 * 2.数字类型(number)
 * 3.字符串类型(string)
 * 4.数组类型(arrry)
 * 5.元组类型(tuple)
 * 6.枚举类型(enum)
 * 7.任意类型(any)
 * 8.null和undefined
 * 9.voild类型
 * 10.never类型
 */
/**
 * 1.布尔类型：true/false
 */
let flag = true;
flag = false;
/**
 * 2.数组类型：number
 */
let flag2 = 123;
flag2 = 12.3;
/**
 * 3.字符串类型：string
 */
let flag3 = 'hello';
/**
 * 4.数组类型：array
 */
// 4.1 定义数组方法一
let flag4 = [1, 2, 3]; // 声明数组的类型是数字数组
let flag5 = ['ts', 'js', 'java']; // 声明数组的类型是字符串数组
// 4.2 定义数组方法er
let flag6 = [1, 2, 3];
let flag7 = ['ts', 'js', 'java'];
// 4.3 定义数组方法三
let flagn = [1, 2, '3', true];
/**
 * 5. 元组类型：属于数组的一种,可以指定数组的类型
 */
let flag8 = ['hello', 1, false];
/**
 * 6. 枚举类型：定义标识符
 * pay_status 0 已支付 1 未支付 2 取消支付
 */
var payStatus;
(function (payStatus) {
    payStatus[payStatus["success"] = 0] = "success";
    payStatus[payStatus["error"] = 1] = "error";
    payStatus[payStatus["cancel"] = 2] = "cancel";
})(payStatus || (payStatus = {}));
let flag9 = payStatus.success;
console.log('flag9:', flag9);
// 要注意的： 官方例子：
// 实例一：
var color;
(function (color) {
    color[color["red"] = 0] = "red";
    color[color["bule"] = 1] = "bule";
    color[color["orange"] = 2] = "orange";
})(color || (color = {}));
let flag10 = color.red; // 没有赋值直接去下标 0 
let flag11 = color.bule; // 没有赋值直接去下标 1
let flag12 = color.orange; // 没有赋值直接去下标 2
// 实例二：
var Color;
(function (Color) {
    Color[Color["red"] = 0] = "red";
    Color[Color["bule"] = 5] = "bule";
    Color[Color["orange"] = 6] = "orange";
    Color[Color["skybule"] = 7] = "skybule";
})(Color || (Color = {}));
let flag13 = Color.red; // 没有赋值直接去下标 0 
let flag14 = Color.bule; // 5
console.log('flag14:', flag14);
let flag15 = Color.orange; //在上一个基础上加一 6
console.log('flag15:', flag15);
let flag16 = Color.skybule;
console.log('flag16:', flag16);
/**
 * 7.任意类型：any
 */
var flag17 = 123;
flag17 = 'hello';
flag17 = [1, 2, 3, 4, 5];
/**
 * 8.null 和 undefined 其他(never)类型的字类型
 */
// let flag18:number
// console.log(flag18); //输出undefined 报错 可以像下面这样去写
let flag18;
console.log(flag18); //输出undefined  不报错
let flag19;
console.log(flag19); // 输出undefined 不报错
// 当一个元素可能是number,null,undefined
let flag20;
/**
 * 9.void类型：表示没有任何类型，一般用于定义方法没有返回值
 */
// ES5的定义方法
function run() {
    console.log('run');
}
run();
//ts中：不存在返回值
function run2() {
    console.log(111);
}
run2();
// ts 存在返回值：
function run3() {
    return 111;
}
run3();
/**
 * 10.never:声明never变量只能被never类型赋值
 * never表示从来不会出现的值
 * 包含undefined&null,一般用不到这种
 */
let flag21;
flag21 = undefined;
let flag22;
flag22 = null;
let flag23;
flag23 = (() => {
    throw new Error('c错误');
})();
